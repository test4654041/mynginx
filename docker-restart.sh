# подключаемся по ssh
ssh dockeruser@192.168.0.130
# переходим в директорию с файлом docker-compose
cd /honme/PiUser/mynginx
# логинимся к репозиторию registry.gitlab.com
docker login registry.gitlab.com
# обновляем локальный образ сервиса app-nginx из файла docker-compose
docker-compose pull app-nginx
# перезапускаем контейнер
docker-compose up -d